﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/slider.css">
    <link href='http://fonts.googleapis.com/css?family=Passion+One:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/tms-0.4.1.js"></script>
    <script>
		$(document).ready(function(){
			$('.slider')._TMS({
				show:0,
				pauseOnHover:false,
				prevBu:false,
				nextBu:false,
				playBu:false,
				duration:1000,
				preset:'fade',
				pagination:true,//'.pagination',true,'<ul></ul>'
				pagNums:false,
				slideshow:8000,
				numStatus:false,
				banners:'fade',// fromLeft, fromRight, fromTop, fromBottom
				waitBannerAnimation:false,
				progressBar:false
			})		
	 	}) 
	</script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
    	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800' rel='stylesheet' type='text/css'>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
<div class="main">

  <!--==============================content================================-->
    <section id="content">
    	<div class="header-block">
            <nav class="nav-bg">  
              <ul class="menu">
                    <li class="current"><a href="index.html">Accueil</a></li>
                    <li><a href="register.html">Inscription</a></li>
                    <li class><a href="search.html">Recherche</a></li>

               </ul>
            </div>   
             
            <div class="grid_10 prefix_7 suffix_7 top-2">
            	<h2 class="h2-1">Our main profile:</h2>
            </div>
            <div class="grid_24">    
                <div class="box-1">
                	<div>
                    	<img src="images/page1-img1.jpg" alt="" class="img-border">
                        <p class="text-1">Сonsulting</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                    <div>
                    	<img src="images/page1-img2.jpg" alt="" class="img-border">
                        <p class="text-1">Start-Up</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                    <div>
                    	<img src="images/page1-img3.jpg" alt="" class="img-border">
                        <p class="text-1">Finance</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                    <div>
                    	<img src="images/page1-img4.jpg" alt="" class="img-border">
                        <p class="text-1">Management</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                    <div>
                    	<img src="images/page1-img5.jpg" alt="" class="img-border">
                        <p class="text-1">Outsourcing</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                    <div class="last">
                    	<img src="images/page1-img6.jpg" alt="" class="img-border">
                        <p class="text-1">Support</p>
                        <p>Vivamus hendrerit mauris ut dui gravida ut viverra lectus tincidunt. Cras mattis tempor eros nec <br>tristique.</p>
                        <a href="#" class="link-1">read more</a>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </section> 
<!--==============================footer=================================-->
  <footer>
      <p><strong>Sport-Meeting</strong> <span>© 2021</span></p>
     
</div>  
</body>
</html>