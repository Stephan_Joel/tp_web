﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>Search</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/cycle.css">
    <link href='http://fonts.googleapis.com/css?family=Passion+One:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.cycle.all.js"></script>
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script>
		jQuery(document).ready(function() {
			$('#s2').cycle({ 
				fx:     'fade', 
				speed:  'slow', 
				pager:  '#nav',
				timeout: 8000, 
			});
		});
	</script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
    	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800' rel='stylesheet' type='text/css'>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
<div class="main">
   
  <!--==============================content================================-->
    <section id="content">
    	<div class="header-block">
            <nav>  
              <ul class="menu">
                <li><a href="index.html">Accueil</a></li>
                <li><a href="register.html">Inscription</a></li>
                <li class="current"><a href="search.html">Recherche</a></li>
                </ul>
            </nav>
          
        </div>
    	<div class="container_24 top-3">
        	<div class="grid_24 align-c">
				
            </div>   
            <div class="grid_16 prefix_1">
              <h2 class="h2-2">Rechercher des partenaires:</h2>
                <hr>
              <form id="form" method="post" >
                <fieldset>
                  <label><strong>Sport <br><span>pratiqué</span></strong>
                    <select name="" id="">
                      <option value="">tennis</option>
                    </select>
                    </label>
                    <label><strong>Niveau</strong>
                      <select name="" id="">
                        <option value="">Debutant</option>
                      </select>
                      </label>
                      <label><strong>Département</strong>
                        <select name="" id="">
                          <option value="" placeholder="">Choissisez</option>
                        </select>
                        </label>
                  <div class="btns"><a href="#" class="link-1" onClick="document.getElementById('form').submit()">Recherche</a></div>
                </fieldset>  
              </form> 
              
          </div>
          
        </div>
    </section> 
    
    <table class="table table-hover">
   
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nom</th>
          <th scope="col">Prénom</th>
          <th scope="col">Département</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
      </tbody>
    </table>
<!--==============================footer=================================-->
  <footer>
      <p><strong>Sport-Meeting</strong> <span>© 2021</span></p>
     
</div>  
</body>
</html>