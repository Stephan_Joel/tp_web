﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>Resistration</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="screen" href="css/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/grid.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/cycle.css">
    <link href='http://fonts.googleapis.com/css?family=Passion+One:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.cycle.all.js"></script>
    <script>
		jQuery(document).ready(function() {
			$('#s2').cycle({ 
				fx:     'fade', 
				speed:  'slow', 
				pager:  '#nav',
				timeout: 8000, 
			});
		});
	</script>
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
    	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800' rel='stylesheet' type='text/css'>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
</head>
<body>
<div class="main">
  
  <!--==============================content================================-->
    <section id="content">
    	<div class="header-block">
            <nav>  
              <ul class="menu">
                <li><a href="index.html">Accueil</a></li>
                <li class="current"><a href="register.html">Inscription</a></li>
                <li><a href="search.html">Recherche</a></li>
                </ul>
            </nav>
            <div class="cycle">
                <div id="s2">
                  <div><div class="banner">Register in order to<span>Access To your Account</span></div></div>  
                  <div><div class="banner">We invest our mind resources<span>in order to link you with all the world</span></div></div>
           	    </div>
              <div id="nav"></div>
            </div>
        </div>
    	<div class="container_24 top-2">  
            <div class="grid_7">
                <h2 class="h2-4"></h2>
                <div class="map img-border">
                  <img src="images/reg.jpg" alt="" width="270" height="205">
                </div>

            </div>
            <div class="grid_16 prefix_1">
                <h2 class="h2-6">Sign In</h2>
                <hr>
                <form id="form" method="post" >
                  <fieldset>
                    <label><strong>Name:</strong><input type="text" value=""></label>
                    <label><strong>SurName:</strong><input type="text" value=""></label>
                    <label><strong>Mail:</strong><input type="text" value=""></label>
                    <label><strong>Password:</strong><input type="text" value=""></label>
                    <label><strong>Confirm:</strong><input type="text" value=""></label>
                    <div class="btns"><a href="">Login ?</a><a href="#" class="link-1" onClick="document.getElementById('form').submit()">Register</a></div>
                  </fieldset>  
                </form> 
            </div>
            <div class="clear"></div>
        </div>
    </section> 
<!--==============================footer=================================-->
  <footer>
      <p><strong>Sport-Meeting</strong> <span>© 2021</span></p>
  </footer>	
</div>  
</body>
</html>